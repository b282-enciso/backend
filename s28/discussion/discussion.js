// CRUD Operations

// Create Operation
// insertOne() - Inserts one document to the collection
db.users.insertOne({
	"firstName": "John",
	"lastName": "Smith"
});

// insertMany() - Inserts multiple documents to the collection
db.users.insertMany([
	{ "firstName": "John", "lastName": "Doe" },
	{ "firstName": "Jane", "lastName": "Doe" }
]);

// Read Operation
// find() - get all the inserted users
db.users.find();

// Retrieving specific documents
db.users.find({ "lastName": "Doe" });

// Update Operation
// updateOne() - modify one document
db.users.updateOne(
	{
		"_id": ObjectId("648af8e691421fbaa8335598")
	},
	{
		$set: {
			"email": "johnsmith@gmail.com"
		}
	}
);

// updateMany() - modify multiple documents
db.users.updateMany(
	{
		"lastName": "Doe"
	},
	{
		$set: {
			"isAdmin": false
		}
	}
);

// Delete Operation
// deleteMany - deletes multiple documents
db.users.deleteMany({ "lastName": "Doe" });

// deleteOne - deletes single document
db.users.deleteOne({ "_id": ObjectId("648af8e691421fbaa8335598") });