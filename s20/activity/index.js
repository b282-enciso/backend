// console.log ("Hello World!")
// console.log("Hello World");

//Objective 1
let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

//Add code here

let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

for (let i = 0; i < string.length; i++) {
  let letter = string[i];

  if (isVowel(letter)) {
    continue;
  } else {
    filteredString += letter;
  }
}

console.log(filteredString);

function isVowel(letter) {
  let vowels = ['a', 'e', 'i', 'o', 'u'];
  return vowels.includes(letter.toLowerCase());
}



//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        filteredString: typeof filteredString !== 'undefined' ? filteredString : null

    }
} catch(err){

}